import 'package:ventas_3342/model/entity/ventas.dart';
import 'package:ventas_3342/model/repository/backend.dart';
import 'package:ventas_3342/model/repository/firebase_storage.dart';
import 'package:ventas_3342/model/repository/ventas.dart';

class CobrosController {
  late final BackendRepository _backend;
  late final VentasRepository _ventasRepository;
  late final FirebaseStorageRepository _storageRepository;

  CobrosController() {
    _backend = BackendRepository();
    _ventasRepository = VentasRepository();
    _storageRepository = FirebaseStorageRepository();
  }

  Future<int> calcularValorCuota(
      int monto, int numeroCuotas, String periodicidad) async {
    //Validar parametros
    if (monto <= 0 || numeroCuotas <= 0) {
      return 0;
    }

    var valor =
        await _backend.obtenerValorCuota(monto, numeroCuotas, periodicidad);
    return valor;
  }

  Future<void> nuevaVenta(VentasEntity venta) async {
    // si tiene foto
    if (venta.foto != null) {
      // subir la foto al storage y obtener la URL
      var photoUrl =
          await _storageRepository.loadFile(venta.foto!, "sales/photo");
      // cambio la URL de la foto en la venta
      venta.foto = photoUrl;
    }

    await _ventasRepository.nuevaVenta(venta);
  }
}
