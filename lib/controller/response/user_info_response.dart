class UserInfoResponse {
  final String email;
  final String name;
  final bool isAdmin;

  const UserInfoResponse(
      {required this.email, required this.name, required this.isAdmin});
}
