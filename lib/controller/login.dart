import '../model/entity/usuarios.dart';
import '../model/repository/firebaseauth.dart';
import '../model/repository/usuarios.dart';
import 'request/login_request.dart';
import 'request/signup_request.dart';
import 'response/user_info_response.dart';

class LoginController {
  late UsuarioReporitory _reporitory;
  late FirebaseAuthRepository _authRepository;

  LoginController() {
    _reporitory = UsuarioReporitory();
    _authRepository = FirebaseAuthRepository();
  }

  Future<String> validarCorreoClave(LoginRequest request) async {
    final uid = await _authRepository.signInWithEmailPassword(
        request.correo!, request.clave!);

    return uid;
  }

  Future<void> cerrarSesion() async {
    _authRepository.signOut();
  }

  Future<void> registrar(SignUpRequest model,
      {bool isAdminUser = false}) async {
    final uid = await _authRepository.createAuthUserEmailPassword(
        model.email, model.password);

    _reporitory.guardarUsuario(
        uid,
        UsuarioEntity(model.email,
            nombre: model.name,
            telefono: model.phoneNumber,
            direccion: model.address,
            isAdmin: isAdminUser));
  }

  Future<UserInfoResponse> consultarUsuario(String uid) async {
    var user = await _reporitory.consultarUsuarioPorUid(uid);

    return UserInfoResponse(
      email: user.correo,
      name: user.nombre!,
      isAdmin: user.isAdmin!,
    );
  }
}
