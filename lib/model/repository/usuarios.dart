import 'package:cloud_firestore/cloud_firestore.dart';

import '../entity/usuarios.dart';

class UsuarioReporitory {
  late final CollectionReference _collection;

  UsuarioReporitory() {
    final db = FirebaseFirestore.instance;
    _collection = db.collection("users");
  }

  Future<UsuarioEntity> consultarUsuarioPorCorreo(String uid) async {
    final doc = await _collection
        .doc(uid)
        .withConverter<UsuarioEntity>(
            fromFirestore: UsuarioEntity.fromFirestore,
            toFirestore: (value, options) => value.toFirestore())
        .get();

    var dato = doc.data();

    if (dato == null) {
      throw Exception("Usuario no existente");
    }
    return dato;
  }

  Future<UsuarioEntity> consultarUsuarioPorUid(String uid) async {
    final doc = await _collection
        .doc(uid)
        .withConverter<UsuarioEntity>(
            fromFirestore: UsuarioEntity.fromFirestore,
            toFirestore: (value, options) => value.toFirestore())
        .get();

    var dato = doc.data();

    if (dato == null) {
      throw Exception("Usuario no existente");
    }
    return dato;
  }

  Future<void> guardarUsuario(String uid, UsuarioEntity usuario) async {
    _collection.doc(uid).set(usuario.toFirestore());
  }
}
