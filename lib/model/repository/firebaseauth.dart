import 'package:firebase_auth/firebase_auth.dart';

class FirebaseAuthRepository {
  Future<String> createAuthUserEmailPassword(
      String emailAddress, String password) async {
    try {
      final credential =
          await FirebaseAuth.instance.createUserWithEmailAndPassword(
        email: emailAddress,
        password: password,
      );

      return credential.user!.uid;
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        return Future.error("La clave es demasiado debil");
      } else if (e.code == 'email-already-in-use') {
        return Future.error("ya existe un usuario con este correo electrónico");
      } else {
        return Future.error(e.message!);
      }
    }
  }

  Future<String> signInWithEmailPassword(
      String emailAddress, String password) async {
    try {
      final credential = await FirebaseAuth.instance.signInWithEmailAndPassword(
        email: emailAddress,
        password: password,
      );

      return credential.user!.uid;
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        return Future.error("El usuario no existe");
      } else if (e.code == 'wrong-password') {
        return Future.error("Credenciales inválidas");
      } else {
        return Future.error(e.message!);
      }
    }
  }

  Future<void> signOut() async {
    await FirebaseAuth.instance.signOut();
  }
}
