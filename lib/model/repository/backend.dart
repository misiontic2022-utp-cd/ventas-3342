import 'dart:convert';

import 'package:http/http.dart' as http;

class BackendRepository {
  Future<int> obtenerValorCuota(
      int monto, int numeroCuotas, String periodicidad) async {
    var datos = {
      "monto": monto,
      "numeroCuotas": numeroCuotas,
      "periodicidad": periodicidad
    };

    var response = await http.post(
        Uri.https("ventas-3342.herokuapp.com", "/api/sales/part"),
        headers: {"Content-Type": "application/json"},
        body: json.encode(datos));

    if (response.statusCode != 200) {
      var error = json.decode(response.body)["error"];
      throw Exception(error);
    }
    var valor = json.decode(response.body)["valor"];
    return valor;
  }
}
