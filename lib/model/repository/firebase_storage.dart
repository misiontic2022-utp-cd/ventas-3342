import 'dart:io';

import 'package:firebase_storage/firebase_storage.dart';

class FirebaseStorageRepository {
  late final FirebaseStorage _storage;

  FirebaseStorageRepository() {
    _storage = FirebaseStorage.instanceFor(
        bucket: "gs://mt2022-ventas-3342.appspot.com");
  }

  Future<String> loadFile(String filePath, String remoteDirectory) async {
    var file = File(filePath);

    // Obntener el nombre del archivo
    var index = filePath.lastIndexOf("/") + 1;
    var fileName = filePath.substring(index);

    // Crear referencia donde se va a guardar el archivo
    var remoteFileName = "$remoteDirectory/$fileName";
    var photosRef = _storage.ref(remoteFileName);
    try {
      await photosRef.putFile(file);

      var url = await photosRef.getDownloadURL();
      return url;
    } on FirebaseException catch (e) {
      return Future.error("Error cargando archivo: $e");
    }
  }
}
