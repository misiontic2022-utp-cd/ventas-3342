import 'package:cloud_firestore/cloud_firestore.dart';

import '../entity/ventas.dart';

class VentasRepository {
  late final CollectionReference _collection;

  VentasRepository() {
    _collection = FirebaseFirestore.instance.collection("sales");
  }

  Future<List<VentasEntity>> listar(String uid) async {
    final query = await _collection
        .withConverter<VentasEntity>(
            fromFirestore: VentasEntity.fromFirestore,
            toFirestore: (value, options) => value.toFirestore())
        .where("usuario", isEqualTo: uid)
        .get();

    final ventas = query.docs.cast().map<VentasEntity>((e) => e.data());

    return ventas.toList();
  }

  Future<void> nuevaVenta(VentasEntity venta) async {
    await _collection
        .withConverter<VentasEntity>(
            fromFirestore: VentasEntity.fromFirestore,
            toFirestore: (value, options) => value.toFirestore())
        .add(venta);
  }
}
