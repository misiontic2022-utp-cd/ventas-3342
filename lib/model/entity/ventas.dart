import 'package:cloud_firestore/cloud_firestore.dart';

import 'pagos.dart';

class VentasEntity {
  late String? foto;
  late String? usuario;
  late String? cliente;
  late String? direccion;
  late String? telefono;
  late double? monto;
  late int? coutas;
  late String? periodicidad;
  late double? valor;

  // Posicion geográfica
  late double? lat;
  late double? long;

  late List<PagosEntity> pagos;

  VentasEntity(
      {this.usuario,
      this.foto,
      this.cliente,
      this.direccion,
      this.telefono,
      this.monto,
      this.coutas,
      this.periodicidad,
      this.valor,
      this.lat,
      this.long}) {
    pagos = <PagosEntity>[];
  }

  factory VentasEntity.fromFirestore(
      DocumentSnapshot<Map<String, dynamic>> snapshot,
      SnapshotOptions? options) {
    final data = snapshot.data();
    final venta = VentasEntity(
        foto: data?["foto"],
        usuario: data?["usuario"],
        cliente: data?["cliente"],
        direccion: data?["direccion"],
        telefono: data?["telefono"],
        monto: data?["monto"],
        coutas: data?["coutas"],
        periodicidad: data?["periodicidad"],
        valor: data?["valor"],
        lat: data?["lat"],
        long: data?["long"]);

    // TODO: convertir la lista de pagos

    return venta;
  }

  Map<String, dynamic> toFirestore() {
    return {
      "usuario": usuario,
      if (foto != null) "foto": foto,
      if (cliente != null) "cliente": cliente,
      if (direccion != null) "direccion": direccion,
      if (telefono != null) "telefono": telefono,
      if (monto != null) "monto": monto,
      if (coutas != null) "coutas": coutas,
      if (periodicidad != null) "periodicidad": periodicidad,
      if (valor != null) "valor": valor,
      if (lat != null) "lat": lat,
      if (long != null) "long": long,
      // TODO: convertir la lista de pagos
    };
  }
}
