import 'package:cloud_firestore/cloud_firestore.dart';

class UsuarioEntity {
  late String correo;
  late String? nombre;
  late String? direccion;
  late String? telefono;
  late bool? isAdmin;

  UsuarioEntity(this.correo,
      {this.nombre, this.direccion, this.telefono, this.isAdmin});

  factory UsuarioEntity.fromFirestore(
      DocumentSnapshot<Map<String, dynamic>> snapshot,
      SnapshotOptions? options) {
    final data = snapshot.data();
    return UsuarioEntity(data?["correo"],
        nombre: data?["nombre"],
        direccion: data?["direccion"],
        telefono: data?["telefono"],
        isAdmin: data?["isAdmin"]);
  }

  Map<String, dynamic> toFirestore() {
    return {
      "correo": correo,
      if (nombre != null) "nombre": nombre,
      if (direccion != null) "direccion": direccion,
      if (telefono != null) "telefono": telefono,
      "isAdmin": isAdmin ?? false
    };
  }
}
