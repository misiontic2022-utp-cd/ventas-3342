import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:ventas_3342/view/pages/payments.dart';
import 'firebase_options.dart';

import 'view/pages/login.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  // Habilitar errores en crashlytics
  FlutterError.onError = (details) {
    FirebaseCrashlytics.instance.recordFlutterFatalError(details);
  };
  PlatformDispatcher.instance.onError = (exception, stackTrace) {
    FirebaseCrashlytics.instance
        .recordError(exception, stackTrace, fatal: true);
    return true;
  };

  await initializeDateFormatting("es", "");

  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late Future<SharedPreferences> _pref;
  String? _uid;

  _MyAppState() {
    _pref = SharedPreferences.getInstance();
    _uid = null;
  }

  @override
  void initState() {
    super.initState();

    _pref.then(
      (pref) => setState(() {
        _uid = pref.getString("uid");
      }),
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Ventas a domicilio",
      theme: ThemeData(
        appBarTheme: const AppBarTheme(color: Colors.red),
        scaffoldBackgroundColor: Colors.amber[100],
      ),
      home: _selectPage(),
    );
  }

  Widget _selectPage() {
    if (_uid == null) {
      return LoginPage();
    } else {
      return PaymentsPage();
    }
  }
}
