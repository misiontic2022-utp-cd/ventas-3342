import 'dart:io';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../model/entity/ventas.dart';
import '../../model/repository/ventas.dart';
import '../widgets/drawer.dart';
import 'new_sale.dart';

class PaymentsPage extends StatefulWidget {
  late final VentasRepository _repository;

  PaymentsPage({super.key}) {
    _repository = VentasRepository();
  }

  @override
  State<PaymentsPage> createState() => _PaymentsPageState();
}

class _PaymentsPageState extends State<PaymentsPage> {
  String? _uid;
  final Future<SharedPreferences> _pref = SharedPreferences.getInstance();
  final _titulo = "Ventas";
  late List<VentasEntity> _lista;

  _PaymentsPageState() {
    _uid = null;
    _lista = [];
  }

  @override
  void initState() {
    super.initState();

    _pref.then(
      (pref) => setState(() {
        _uid = pref.getString("uid");
        _listarCobros(_uid!);
      }),
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text(_titulo),
        ),
        drawer: VentasDrawer(),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _fechaActual(),
              const Text(
                "Cobros",
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(height: 8),
              _lista.isEmpty
                  ? const Text("No hay cobros para hoy")
                  : Expanded(
                      child: ListView.builder(
                          itemCount: _lista.length,
                          itemBuilder: ((context, index) {
                            final item = _lista[index];

                            final CircleAvatar avatar;
                            if (_lista[index].foto != null) {
                              ImageProvider provider;
                              if (_lista[index].foto!.startsWith("http")) {
                                provider = NetworkImage(_lista[index].foto!);
                              } else {
                                provider = FileImage(File(_lista[index].foto!));
                              }

                              avatar = CircleAvatar(
                                backgroundImage: provider,
                              );
                            } else {
                              avatar = const CircleAvatar();
                            }
                            return ListTile(
                              leading: avatar,
                              trailing: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  (item.telefono == null
                                      ? const Text("")
                                      : IconButton(
                                          icon: const Icon(Icons.phone),
                                          onPressed: () async {
                                            // Hacer la llamada al usuario
                                            var url = Uri(
                                              scheme: 'tel',
                                              path: item.telefono,
                                            );
                                            if (await canLaunchUrl(url)) {
                                              launchUrl(url);
                                            }
                                          },
                                        )),
                                  (item.lat == null
                                      ? const Text("")
                                      : IconButton(
                                          icon: const Icon(Icons.pin_drop),
                                          onPressed: () async {
                                            // Abrir la direccion hasta ubicacion
                                            // var url = Uri(
                                            //   scheme: "google.navigation",
                                            //   path:
                                            //       "q=${item.lat},${item.long}&mode=d",
                                            // );
                                            var url = Uri(
                                              scheme: "geo",
                                              path: "${item.lat},${item.long}",
                                            );
                                            if (await canLaunchUrl(url)) {
                                              launchUrl(url);
                                            }
                                          },
                                        )),
                                ],
                              ),
                              title: Text(item.cliente!),
                              subtitle: Text(item.direccion!),
                              onTap: () {},
                            );
                          })),
                    ),
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton(
          child: const Icon(Icons.add_shopping_cart),
          onPressed: () async {
            await Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => NewSalePage(uid: _uid!),
              ),
            );

            if (!mounted) return;

            _listarCobros(_uid!);
          },
        ),
      ),
    );
  }

  Widget _fechaActual() {
    var ahora = DateTime.now();
    var formatter = DateFormat("EEEE, d 'de' MMMM 'de' y", "es");

    var fecha = formatter.format(ahora);
    return Center(
      child: Text(
        fecha,
        style: const TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 18,
        ),
      ),
    );
  }

  void _listarCobros(String uid) {
    // Lista de cobros a realizar
    widget._repository.listar(uid).then(
          (value) => setState(
            () {
              _lista = value;
            },
          ),
        );
  }
}
