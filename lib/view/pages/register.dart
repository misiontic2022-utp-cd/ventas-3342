import 'package:flutter/material.dart';
import 'package:ventas_3342/controller/login.dart';
import 'package:ventas_3342/controller/request/signup_request.dart';

class RegisterPage extends StatelessWidget {
  late final LoginController _controller;
  late final SignUpRequest _model;

  RegisterPage({super.key}) {
    _controller = LoginController();
    _model = SignUpRequest();
  }

  @override
  Widget build(BuildContext context) {
    var formKey = GlobalKey<FormState>();

    return Scaffold(
      appBar: AppBar(
        title: const Text("Registrar usaurio"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Form(
          key: formKey,
          child: SingleChildScrollView(
            child: Column(
              children: [
                _getFormField("Nombre *", 100, _validateRequired, (newValue) {
                  _model.name = newValue!;
                }, type: TextInputType.name),
                _getFormField("Correo electronico *", 200, _validateRequired,
                    (newValue) {
                  _model.email = newValue!;
                }, type: TextInputType.emailAddress),
                _getFormField("Direccion *", 100, _validateRequired,
                    (newValue) {
                  _model.address = newValue!;
                }, type: TextInputType.streetAddress),
                _getFormField("Telefono", 15, (value) => null, (newValue) {
                  _model.phoneNumber = newValue;
                }, type: TextInputType.phone),
                _getFormField("Clave *", 20, _validateRequired, (newValue) {
                  _model.password = newValue!;
                }, isPassword: true),
                ElevatedButton(
                  child: const Text("Registrar"),
                  onPressed: () {
                    if (formKey.currentState!.validate()) {
                      formKey.currentState!.save();

                      _controller.registrar(_model).then((value) {
                        ScaffoldMessenger.of(context).showSnackBar(
                            const SnackBar(
                                content:
                                    Text("Registro realizado correctamente")));
                        Navigator.pop(context);
                      }).catchError((e) {
                        ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(content: Text(e.toString())));
                      });
                    }
                  },
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  String? _validateRequired(String? value) {
    if (value == null || value.isEmpty) {
      return "El campo es obligatorio";
    }
    return null;
  }

  Widget _getFormField(String title, int length,
      FormFieldValidator<String?> validator, FormFieldSetter<String?> save,
      {TextInputType type = TextInputType.text, bool isPassword = false}) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 8.0),
      child: TextFormField(
        maxLength: length,
        keyboardType: type,
        obscureText: isPassword,
        decoration: InputDecoration(
          border: const OutlineInputBorder(),
          labelText: title,
        ),
        validator: validator,
        onSaved: save,
      ),
    );
  }
}
