import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:location/location.dart';
import 'package:number_text_input_formatter/number_text_input_formatter.dart';

import '../../controller/cobros.dart';
import '../../model/entity/ventas.dart';
import '../widgets/take_photo.dart';

class NewSalePage extends StatelessWidget {
  late final String uid;
  late final VentasEntity _venta;
  late final CobrosController _controller;

  NewSalePage({super.key, required this.uid}) {
    _controller = CobrosController();
    _venta = VentasEntity(usuario: uid);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Nueva venta"),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                "Nueva venta",
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(height: 8),
              _formulario(context),
            ],
          ),
        ),
      ),
    );
  }

  Widget _formulario(BuildContext context) {
    final formkey = GlobalKey<FormState>();
    return Form(
      key: formkey,
      child: Column(children: [
        _cliente(context),
        const SizedBox(height: 8),
        _direccion(),
        const SizedBox(height: 8),
        _telefono(),
        const SizedBox(height: 8),
        _CamposValores(
          formKey: formkey,
          venta: _venta,
          controller: _controller,
        ),
        const SizedBox(height: 8),
        _botonEnvio(context, formkey),
      ]),
    );
  }

  Widget _cliente(BuildContext context) {
    return TextFormField(
      autofocus: true,
      keyboardType: TextInputType.name,
      maxLength: 100,
      decoration: InputDecoration(
        icon: IconoCliente(venta: _venta),
        border: const OutlineInputBorder(),
        labelText: 'Cliente',
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "El cliente es un campo obligatorio";
        }
        return null;
      },
      onSaved: (newValue) {
        _venta.cliente = newValue;
      },
    );
  }

  Widget _direccion() {
    return TextFormField(
      maxLength: 200,
      keyboardType: TextInputType.streetAddress,
      decoration: const InputDecoration(
        border: OutlineInputBorder(),
        labelText: 'Direccion',
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "La dirección es un campo obligatorio";
        }
        return null;
      },
      onSaved: (newValue) {
        _venta.direccion = newValue;
      },
    );
  }

  Widget _telefono() {
    return TextFormField(
      maxLength: 15,
      keyboardType: TextInputType.phone,
      decoration: const InputDecoration(
        border: OutlineInputBorder(),
        labelText: 'Telefono',
      ),
      onSaved: (newValue) {
        _venta.telefono = newValue;
      },
    );
  }

  Widget _botonEnvio(BuildContext context, GlobalKey<FormState> formkey) {
    return ElevatedButton(
      child: const Text("Guardar"),
      onPressed: () async {
        if (formkey.currentState!.validate()) {
          formkey.currentState!.save();

          try {
            var nav = Navigator.of(context);
            var mess = ScaffoldMessenger.of(context);

            // Captura la ubicacion
            await _obtenerUbicacionGeografica();

            // Guardar los datos de la nueva venta
            await _controller.nuevaVenta(_venta);

            // Mostrar mensaje de venta realizada con exito
            mess.showSnackBar(
              const SnackBar(
                content: Text("Venta realizada con exito"),
              ),
            );

            // Volver a la pantalla anterior
            nav.pop();
          } catch (e) {
            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(
                content: Text("Error: $e"),
              ),
            );
          }
        }
      },
    );
  }

  Future<void> _obtenerUbicacionGeografica() async {
    final location = Location();

    var serviceEnabled = await location.serviceEnabled();
    if (!serviceEnabled) {
      serviceEnabled = await location.requestService();
      if (!serviceEnabled) {
        return;
      }
    }

    var permissionGranted = await location.hasPermission();
    if (permissionGranted == PermissionStatus.denied) {
      permissionGranted = await location.requestPermission();
      if (permissionGranted != PermissionStatus.granted) {
        return;
      }
    }

    final locationData = await location.getLocation();

    _venta.lat = locationData.latitude;
    _venta.long = locationData.longitude;
  }
}

class _CamposValores extends StatefulWidget {
  final GlobalKey<FormState> formKey;
  final CobrosController controller;
  final VentasEntity venta;
  const _CamposValores({
    required this.formKey,
    required this.venta,
    required this.controller,
  });

  @override
  State<_CamposValores> createState() => _CamposValoresState();
}

class _CamposValoresState extends State<_CamposValores> {
  final txtValor = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        _monto(context),
        const SizedBox(height: 8),
        Row(
          children: [
            _numeroCuotas(context),
            const SizedBox(width: 8),
            _periodicidad(context),
          ],
        ),
        const SizedBox(height: 16),
        _valorCuota(),
      ],
    );
  }

  TextInputFormatter _numberFormatter() {
    return NumberTextInputFormatter(
      decimalDigits: 0,
      integerDigits: 10,
      groupDigits: 3,
      groupSeparator: '.',
      decimalSeparator: ',',
      allowNegative: false,
    );
  }

  Widget _monto(BuildContext context) {
    return TextFormField(
      keyboardType: TextInputType.number,
      textAlign: TextAlign.right,
      initialValue: "0",
      inputFormatters: [
        _numberFormatter(),
      ],
      decoration: const InputDecoration(
        border: OutlineInputBorder(),
        labelText: 'Monto',
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "El monto es un campo obligatorio";
        }
        value = value.replaceAll('.', '');
        if (int.parse(value) <= 0) {
          return "El monto de la venta no puede ser 0 o negativo";
        }
        return null;
      },
      onChanged: (value) {
        value = value.replaceAll('.', '');
        if (value.isEmpty || int.parse(value) <= 0) {
          return;
        }
        widget.venta.monto = double.parse(value);

        _calcularValorCuota(context);
      },
      onSaved: (newValue) {
        newValue = newValue?.replaceAll('.', '');
        widget.venta.monto = double.tryParse(newValue!);
      },
    );
  }

  Widget _numeroCuotas(BuildContext context) {
    return Expanded(
      child: TextFormField(
        keyboardType: TextInputType.number,
        textAlign: TextAlign.right,
        initialValue: "1",
        inputFormatters: [
          _numberFormatter(),
        ],
        decoration: const InputDecoration(
          border: OutlineInputBorder(),
          labelText: 'Numero cuotas',
        ),
        validator: (value) {
          if (value == null || value.isEmpty) {
            return "El número de cuotas es un campo obligatorio";
          }
          value = value.replaceAll('.', '');
          if (int.parse(value) <= 0) {
            return "El número de cuotas no puede ser 0 o negativo";
          }
          return null;
        },
        onChanged: (value) {
          if (value.isEmpty || int.parse(value) <= 0) {
            return;
          }
          widget.venta.coutas = int.parse(value);

          _calcularValorCuota(context);
        },
        onSaved: (newValue) {
          newValue = newValue?.replaceAll('.', '');
          widget.venta.coutas = int.tryParse(newValue!);
        },
      ),
    );
  }

  Widget _periodicidad(BuildContext context) {
    final opciones = ["Diario", "Semanal", "Quincenal", "Mensual"];
    var valor = opciones[0];

    return Expanded(
      child: DropdownButtonFormField(
        value: valor,
        decoration: const InputDecoration(
          border: OutlineInputBorder(),
          labelText: 'Periodicidad',
        ),
        items: opciones.map<DropdownMenuItem<String>>((String value) {
          return DropdownMenuItem<String>(
            value: value,
            child: Text(value),
          );
        }).toList(),
        onChanged: (value) {
          widget.venta.periodicidad = value;

          _calcularValorCuota(context);
        },
      ),
    );
  }

  Widget _valorCuota() {
    return TextFormField(
      enabled: false,
      textAlign: TextAlign.right,
      controller: txtValor,
      inputFormatters: [
        _numberFormatter(),
      ],
      decoration: const InputDecoration(
        border: OutlineInputBorder(),
        labelText: 'Valor Cuota',
      ),
      onSaved: (newValue) {
        newValue = newValue?.replaceAll('.', '');
        widget.venta.valor = double.tryParse(newValue!);
      },
    );
  }

  void _calcularValorCuota(BuildContext context) {
    var monto = widget.venta.monto?.toInt() ?? 0;
    var numeroCuotas = widget.venta.coutas?.toInt() ?? 1;
    var periodicidad = widget.venta.periodicidad ?? "Diario";

    widget.controller
        .calcularValorCuota(monto, numeroCuotas, periodicidad)
        .then((value) => setState(() {
              widget.venta.valor = value.toDouble();
              // _valorCuotaTexto = value.toString();
              var formatter = NumberFormat("#,###", "es");
              txtValor.text = formatter.format(value);
            }))
        .catchError((error) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text("Error: $error"),
        ),
      );
    });
  }
}

class IconoCliente extends StatefulWidget {
  final VentasEntity venta;
  const IconoCliente({super.key, required this.venta});

  @override
  State<IconoCliente> createState() => _IconoClienteState();
}

class _IconoClienteState extends State<IconoCliente> {
  @override
  Widget build(BuildContext context) {
    Widget icono;
    if (widget.venta.foto == null) {
      icono = IconButton(
        icon: const Icon(Icons.camera_alt_outlined),
        onPressed: () async {
          final nav = Navigator.of(context);

          final cameras = await availableCameras();
          final camera = cameras.first;

          // Abrir la camara
          var imagePath = await nav.push<String?>(MaterialPageRoute(
            builder: (context) => TakePhotoWidget(camera: camera),
          ));
          if (imagePath != null && imagePath.isNotEmpty) {
            setState(() {
              widget.venta.foto = imagePath;
            });
          }
        },
      );
    } else {
      icono = CircleAvatar(
        backgroundImage: FileImage(File(widget.venta.foto!)),
      );
    }
    return icono;
  }
}
