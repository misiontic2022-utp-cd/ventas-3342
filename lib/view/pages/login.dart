import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../controller/login.dart';
import '../../controller/request/login_request.dart';
import 'payments.dart';
import 'register.dart';

class LoginPage extends StatelessWidget {
  late final SharedPreferences _prefs;
  final _logoUrl = "assets/images/ventas.jpg";
  late final LoginController _controller;
  late final LoginRequest _request;

  LoginPage({super.key}) {
    _controller = LoginController();
    _request = LoginRequest();

    SharedPreferences.getInstance().then((value) {
      _prefs = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              _logo(),
              _formulario(context),
              _inicioAlternativo(),
              _registro(context)
            ],
          ),
        ),
      ),
    );
  }

  Widget _logo() {
    return Column(
      children: [
        const SizedBox(
          height: 50,
        ),
        Image.asset(_logoUrl),
        const SizedBox(
          height: 50,
        ),
      ],
    );
  }

  Widget _formulario(BuildContext context) {
    final formKey = GlobalKey<FormState>();

    return Form(
      key: formKey,
      child: Column(
        children: [
          _textoEmail(),
          const SizedBox(
            height: 8,
          ),
          _textoClave(),
          const SizedBox(
            height: 16,
          ),
          ElevatedButton(
            child: const Text("Iniciar Sesión"),
            onPressed: () {
              if (formKey.currentState!.validate()) {
                formKey.currentState!.save();

                // Validar el correo y contraseña
                _controller.validarCorreoClave(_request).then((uid) {
                  _prefs.setString("uid", uid);

                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => PaymentsPage(),
                      ));
                }).catchError((e) {
                  ScaffoldMessenger.of(context)
                      .showSnackBar(SnackBar(content: Text(e.toString())));
                });
              }
            },
          ),
          const SizedBox(
            height: 16,
          ),
        ],
      ),
    );
  }

  Widget _textoEmail() {
    return TextFormField(
      maxLength: 100,
      keyboardType: TextInputType.emailAddress,
      decoration: const InputDecoration(
        border: OutlineInputBorder(),
        labelText: 'Correo electrónico',
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "El correo electrónico es un campo obligatorio";
        }
        if (!value.contains("@")) {
          return "El correo no tiene un formato válido";
        }
        return null;
      },
      onSaved: (value) {
        _request.correo = value;
      },
    );
  }

  Widget _textoClave() {
    return TextFormField(
      obscureText: true,
      maxLength: 20,
      decoration: const InputDecoration(
        border: OutlineInputBorder(),
        labelText: 'Password',
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "La contraseña es un campo obligatorio";
        }
        if (value.length < 6) {
          return "El tamaño minimo de la contraseña son 6 caracteres";
        }
        return null;
      },
      onSaved: (value) {
        _request.clave = value;
      },
    );
  }

  Widget _inicioAlternativo() {
    return Column(
      children: [
        const Text("O iniciar sesión con:"),
        const SizedBox(
          height: 16,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            ElevatedButton(
              child: const Text("Facebook"),
              onPressed: () {},
            ),
            ElevatedButton(
              child: const Text("Google"),
              onPressed: () {},
            ),
          ],
        )
      ],
    );
  }

  Widget _registro(BuildContext context) {
    return Column(
      children: [
        const SizedBox(
          height: 16,
        ),
        TextButton(
          child: const Text("Si no tienes una cuenta, Registrate aqui"),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => RegisterPage(),
              ),
            );
          },
        ),
      ],
    );
  }
}
