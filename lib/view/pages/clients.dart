import 'package:flutter/material.dart';

import '../widgets/drawer.dart';

class ClientsPage extends StatelessWidget {
  const ClientsPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Clientes"),
      ),
      drawer: VentasDrawer(),
      body: Container(),
    );
  }
}
