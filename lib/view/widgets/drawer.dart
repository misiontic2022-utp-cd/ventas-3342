import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../controller/login.dart';
import '../pages/cash_close.dart';
import '../pages/clients.dart';
import '../pages/sales.dart';
import '../pages/payments.dart';

class VentasDrawer extends StatefulWidget {
  late final LoginController _controller;

  VentasDrawer({super.key}) {
    _controller = LoginController();
  }

  @override
  State<VentasDrawer> createState() => _VentasDrawerState();
}

class _VentasDrawerState extends State<VentasDrawer> {
  late String name;
  late String email;
  final Future<SharedPreferences> _pref = SharedPreferences.getInstance();

  @override
  void initState() {
    super.initState();

    name = "";
    email = "";
    _pref.then((pref) {
      final uid = pref.getString("uid");
      if (uid != null) {
        widget._controller.consultarUsuario(uid).then(
              (value) => setState(() {
                name = value.name;
                email = value.email;
              }),
            );
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            decoration: BoxDecoration(
              color: Theme.of(context).appBarTheme.backgroundColor,
            ),
            child: _contenidoCabecera(),
          ),
          ListTile(
            leading: const Icon(Icons.payment),
            title: const Text('Cobros'),
            onTap: () {
              Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) => PaymentsPage(),
                  ));
            },
          ),
          ListTile(
            leading: const Icon(Icons.shopping_bag),
            title: const Text('Ventas'),
            onTap: () {
              Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const SalesPage(),
                  ));
            },
          ),
          ListTile(
            leading: const Icon(Icons.account_box),
            title: const Text('Clientes'),
            onTap: () {
              Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const ClientsPage(),
                  ));
            },
          ),
          ListTile(
            leading: const Icon(Icons.account_balance),
            title: const Text('Cierre de caja'),
            onTap: () {
              //throw Exception("No pude activar la opcion de cierre de caja");

              Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const CashClosePage(),
                  ));
            },
          ),
          ListTile(
            leading: const Icon(Icons.door_back_door),
            title: const Text('Cerrar sesion'),
            onTap: () {
              var alert = AlertDialog(
                title: const Text("Cerrar sesion"),
                content: const Text("¿Está seguro en cerrar sesión?"),
                actions: [
                  ElevatedButton(
                    child: const Text("Si"),
                    onPressed: () {
                      widget._controller.cerrarSesion().then((value) {
                        _pref.then((pref) {
                          pref.remove("uid");
                        });

                        // cierra Dialog
                        Navigator.pop(context);
                        // cierra Drawer
                        Navigator.pop(context);
                        // cierra Page
                        Navigator.pop(context);
                      }).catchError((e) {
                        ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(
                            content: Text(e.toString()),
                          ),
                        );
                      });
                    },
                  ),
                  TextButton(
                    child: const Text("No"),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                ],
              );

              showDialog(
                context: context,
                builder: (context) => alert,
              );
            },
          ),
        ],
      ),
    );
  }

  Widget _contenidoCabecera() {
    return Row(
      children: [
        const CircleAvatar(child: Icon(Icons.supervisor_account)),
        const SizedBox(
          width: 16,
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              name,
              style: const TextStyle(
                color: Colors.white,
                fontSize: 18,
              ),
            ),
            const SizedBox(
              height: 4,
            ),
            Text(
              email,
              style: const TextStyle(
                color: Colors.white,
              ),
            ),
          ],
        )
      ],
    );
  }
}
